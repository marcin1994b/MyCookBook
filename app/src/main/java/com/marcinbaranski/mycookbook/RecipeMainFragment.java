package com.marcinbaranski.mycookbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class RecipeMainFragment extends Fragment {

    @InjectView(R.id.recipe_img_view) ImageView imageView;
    @InjectView(R.id.recipe_name_text_view) TextView nameTextView;
    @InjectView(R.id.recipe_category_text_view) TextView categoryTextView;
    @InjectView(R.id.recipe_servs_text_view) TextView servsTextView;
    @InjectView(R.id.recipe_preparation_time_text_view) TextView preparationTimeTextView;
    @InjectView(R.id.recipe_cooking_time_text_view) TextView cookingTimeTextView;

    public RecipeMainFragment() {}

    public static RecipeMainFragment newInstance() {
        return new RecipeMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recipe_main, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        MyBitmap myBitmap = (new MyBitmap(getArguments().getString("recipeImagePath")));
        imageView.setImageBitmap(myBitmap.getBitmap());
        nameTextView.setText(getArguments().getString("recipeName"));
        categoryTextView.setText(getResources().getString(R.string.recipe_fragment_category_text_view,
                getArguments().getString("recipeCategoryName")));
        servsTextView.setText(getResources().getString(R.string.recipe_fragment_servs_text_view,
                getArguments().getString("recipeServs")));
        preparationTimeTextView.setText(getResources().getString(R.string.recipe_fragment_prep_time_text_view,
                getArguments().getString("recipePreparationTime")));
        cookingTimeTextView.setText(getResources().getString(R.string.recipe_fragment_cook_time_text_view,
                getArguments().getString("recipeCookingTime")));
    }
}
