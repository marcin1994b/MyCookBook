package com.marcinbaranski.mycookbook;

class Recipe {

    private int id;
    private String name;
    private String category;
    private String servs;
    private String preparationTime;
    private String cookingTime;
    private String ingredients;
    private String formula;

    private String imgPath;

    Recipe(){
    }

    Recipe(int id, String name, String category, String servs, String preparationTime,
           String cookingTime, String ingredients, String formula, String imgPath){

        this.id = id;
        this.name = name;
        this.category = category;
        this.servs = servs;
        this.preparationTime = preparationTime;
        this.cookingTime = cookingTime;
        this.ingredients = ingredients;
        this.formula = formula;
        this.imgPath = imgPath;
    }

    Recipe(String name, String category, String servs, String preparationTime,
           String cookingTime, String ingredients, String formula, String imgPath){

        this.name = name;
        this.category = category;
        this.servs = servs;
        this.preparationTime = preparationTime;
        this.cookingTime = cookingTime;
        this.ingredients = ingredients;
        this.formula = formula;
        this.imgPath = imgPath;
    }

    public int getId(){ return this.id; }

    public String getName(){
        return this.name;
    }

    public String getCategory() { return this.category; }

    public String getServs(){
        return this.servs;
    }

    public String getPreparationTime(){
        return this.preparationTime;
    }

    public String getCookingTime(){
        return this.cookingTime;
    }

    public String getIngredients(){
        return this.ingredients;
    }

    public String getFormula(){
        return this.formula;
    }

    public String getImgPath(){
        return this.imgPath;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setCategory(String category) { this.category = category; }

    public void setServs(String servs){
        this.servs = servs;
    }

    public void setPreparationTime(String preparationTime){
        this.preparationTime = preparationTime;
    }

    public void setCookingTime(String cookingTime){
        this.cookingTime = cookingTime;
    }

    public void setIngredients(String ingredients){
        this.ingredients = ingredients;
    }

    public void setFormula(String formula){
        this.formula = formula;
    }

    public void setImgPath(String imgPath){
        this.imgPath = imgPath;
    }

}
