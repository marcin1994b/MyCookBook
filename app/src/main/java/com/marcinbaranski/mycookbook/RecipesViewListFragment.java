package com.marcinbaranski.mycookbook;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.ArrayList;


public class RecipesViewListFragment extends Fragment {

    private ArrayList<View> recipeViewList;
    private ArrayList<Recipe> recipesList;
    private ArrayList<MyPair<Category, Boolean>> categoriesPairList;

    LinearLayout mainView;

    private View currentViewPress;
    private Point downFingerPoint = new Point();
    private int pressedViewCounter = 0;
    private long downTime = 0;
    int colorDown;
    int colorUp;
    private String recipeOrderType = "";
    private CharSequence[] categoriesArray;
    private boolean[] choosenCategoriesArray;
    MyCookBookActivity mainActivity;

    public RecipesViewListFragment() {}

    public static RecipesViewListFragment newInstance() {
        RecipesViewListFragment fragment = new RecipesViewListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = (LinearLayout) inflater.inflate(R.layout.fragment_recipes_view_list, container, false);
        mainActivity = (MyCookBookActivity) this.getActivity();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.recipes_view_fragment_toolbar_title);
        colorUp = ContextCompat.getColor(this.getContext(), R.color.layoutColorFirst);
        colorDown = ContextCompat.getColor(this.getContext(), R.color.primary_light);
        initCategoriesPairList();
        initSortingCheckBoxes();
        return mainView;
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshViews();
    }

    private void refreshViews(){
        if(recipeViewList != null){
            for(int i = 0; i<recipeViewList.size(); i++){
                ((ViewManager)recipeViewList.get(i).getParent()).removeView(recipeViewList.get(i));
            }
        }
        mainActivity.setLongPressedMode(false);
        pressedViewCounter = 0;
        recipeViewList = new ArrayList<>();
        recipesList = new ArrayList<>();
        recipesList = (new DatabaseHandler(this.getActivity())).getAllRecipes(recipeOrderType, getCheckedCategories());
        addRecipesViews(recipesList);
    }


    private boolean gesturesDetection(View v, MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_CANCEL:
                currentViewPress.setBackgroundColor(colorUp);
                downTime = -1;
                break;
            case MotionEvent.ACTION_DOWN:
                downTime = System.currentTimeMillis();
                downFingerPoint.x = (int)event.getX();
                downFingerPoint.y = (int)event.getRawY();
                currentViewPress = v;
                currentViewPress.setBackgroundColor(colorDown);
                break;
            case MotionEvent.ACTION_UP:
                if(Math.abs(downFingerPoint.x - (int)event.getRawX()) < 100 &&
                        Math.abs(downFingerPoint.y - (int)event.getRawY()) < 50) {
                    long diff = System.currentTimeMillis() - downTime;

                    if (diff > 800 && !mainActivity.isLongPressedMode()) {
                        mainActivity.setLongPressedMode(true);
                        currentViewPress.setPressed(true);
                        pressedViewCounter++;
                        this.getActivity().invalidateOptionsMenu();
                    } else if (mainActivity.isLongPressedMode() && currentViewPress.isPressed()) {
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(false);
                        pressedViewCounter--;
                        if (pressedViewCounter == 0) {
                            mainActivity.setLongPressedMode(false);
                            this.getActivity().invalidateOptionsMenu();
                        }
                    } else if (mainActivity.isLongPressedMode() && !currentViewPress.isPressed()) {
                        currentViewPress.setPressed(true);
                        pressedViewCounter++;
                    } else if (!mainActivity.isLongPressedMode()) {
                        //This is when your tap is short and start new activity.
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(true);
                        Intent intent = new Intent(this.getActivity(), RecipeActivity.class);
                        for (int i = 0; i < recipeViewList.size(); i++) {
                            if (recipeViewList.get(i).isPressed()) {
                                intent.putExtra("recipeID", recipesList.get(i).getId());
                            }
                        }
                        currentViewPress.setPressed(false);
                        startActivity(intent);
                    }
                }else{
                    if(!mainActivity.isLongPressedMode()) {
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(false);
                    }
                }
                break;

        }
        return true;
    }

    // remove datas from database, from recipesList and remove views from List and layout.
    public void remove(){
        DatabaseHandler dbHandler = new DatabaseHandler(this.getActivity());
        int tmp = recipeViewList.size();
        for(int i = 0; i<recipeViewList.size(); i++){
            if(recipeViewList.get(i).isPressed()){
                dbHandler.deleteRecipe(recipesList.get(i).getId());
                ((ViewManager)recipeViewList.get(i).getParent()).removeView(recipeViewList.get(i));
                recipesList.remove(i);
                recipeViewList.remove(i);
                i--;
            }
        }
        mainActivity.setLongPressedMode(false);
        Answers.getInstance().logCustom(new CustomEvent("RemoveRecipeEvent")
                .putCustomAttribute("Number of removed recipes", tmp - recipeViewList.size()));
    }


    //Creat views and add them to layout.
    public void addRecipesViews(ArrayList<Recipe> recipes){
        LayoutInflater inflater = LayoutInflater.from(this.getActivity());

        for(int i = 0; i < recipes.size(); i++){
            LinearLayout view = (LinearLayout) inflater.inflate(R.layout.recipe_box, null, false);

            ((TextView) view.findViewById(R.id.recipeNameView)).setText(recipes.get(i).getName());
            ((TextView) view.findViewById(R.id.categoryView)).setText(recipes.get(i).getCategory());
            ((TextView) view.findViewById(R.id.preparationTimeView)).setText(getResources().getString(R.string.recipe_fragment_prep_time_text_view, recipes.get(i).getPreparationTime()));
            ((TextView) view.findViewById(R.id.cookingTimeView)).setText(getResources().getString(R.string.recipe_fragment_cook_time_text_view, recipes.get(i).getCookingTime()));
            MyBitmap myBitmap = new MyBitmap(recipes.get(i).getImgPath() + "min");
            if(myBitmap.getBitmap() != null) {
                ((ImageView) view.findViewById(R.id.recipeImgView)).setImageBitmap(myBitmap.getBitmap());
            }else{
                ((ImageView) view.findViewById(R.id.recipeImgView)).setImageDrawable(getResources().getDrawable(R.drawable.there_is_no_img));
            }

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gesturesDetection(v, event);
                }});

            LinearLayout list = (LinearLayout) mainView.findViewById(R.id.fragment_recipes_view_list_content);
            list.addView(view);

            recipeViewList.add(view);
        }
    }

    public void cancelButtonPressed(){
        for(int i = 0; i<recipeViewList.size(); i++){
                    recipeViewList.get(i).setPressed(false);
                    recipeViewList.get(i).setBackgroundColor(Color.WHITE);
        }
        mainActivity.setLongPressedMode(false);
        mainActivity.invalidateOptionsMenu();
    }

    public void removeButtonPressed(){
        remove();
        onResume();
    }

    private void initSortingCheckBoxes(){
        choosenCategoriesArray = new boolean[categoriesPairList.size()];
        categoriesArray = new String[categoriesPairList.size()];
        for( int i = 0; i < categoriesPairList.size(); i++){
            categoriesArray[i] = categoriesPairList.get(i).first.getName();
            choosenCategoriesArray[i] = categoriesPairList.get(i).second;
        }
    }

    private void initCategoriesPairList(){
        ArrayList<Category> categories = (new DatabaseHandler(this.getContext())).getAllCategories();
        categoriesPairList = new ArrayList<>();
        for(int i = 0; i < categories.size(); i++){
            categoriesPairList.add(new MyPair(categories.get(i), false));
        }
    }

    public void showSortingSettingsDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.getContext());
        dialog.setTitle(R.string.recipe_view_lists_fragment_sorting_dialog_msg);
        dialog.setMultiChoiceItems(categoriesArray, choosenCategoriesArray, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                choosenCategoriesArray[which] = isChecked;
                categoriesPairList.get(which).second = isChecked;
            }
        });
        dialog.setPositiveButton(R.string.menu_accept_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onResume();
            }
        });
        dialog.show();
    }

    private String[] getCheckedCategories(){
        String result[] = null;
        int index = 0;
        if(categoriesPairList != null && categoriesPairList.size() != 0){
            result = new String[countCheckedCategories()];
            for(int i = 0; i < categoriesPairList.size(); i++){
                if(categoriesPairList.get(i).second) {
                    result[index] = categoriesPairList.get(i).first.getName();
                    index++;
                }
            }
        }
        return result;
    }

    private int countCheckedCategories(){
        int counter = 0;
        for( int i = 0; i < categoriesPairList.size(); i++){
            if(categoriesPairList.get(i).second){
                counter++;
            }
        }
        return counter;
    }


}
