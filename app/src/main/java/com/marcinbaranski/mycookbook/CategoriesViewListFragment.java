package com.marcinbaranski.mycookbook;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class CategoriesViewListFragment extends Fragment {

    LinearLayout mainView;
    MyCookBookActivity mainActivity;
    int index;
    int categorySelectedId;
    ArrayList<View> categoriesViewsList;
    ArrayList<Category> categoriesList;

    public CategoriesViewListFragment() {
        // Required empty public constructor
    }

    public static CategoriesViewListFragment newInstance() {
        CategoriesViewListFragment fragment = new CategoriesViewListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainActivity = (MyCookBookActivity) getActivity();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.categories_fragment_title_label);
        return inflater.inflate(R.layout.fragment_categories_view_list, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshViews();
    }



    void refreshViews(){
        if(categoriesViewsList != null){
            for(int i = 0; i < categoriesViewsList.size(); i++){
                ((ViewManager)categoriesViewsList.get(i).getParent()).removeView(categoriesViewsList.get(i));
            }
        }
        categoriesViewsList = new ArrayList<>();
        categoriesList = new ArrayList<>();
        categoriesList = (new DatabaseHandler(this.getActivity())).getAllCategories();
        addCategoriesViews(categoriesList);
    }

    private void addCategoriesViews(final ArrayList<Category> categoriesList){
        LayoutInflater inflater = LayoutInflater.from(this.getActivity());
        for( int i = 0; i < categoriesList.size(); i++){
            LinearLayout view = (LinearLayout) inflater.inflate(R.layout.category_box, null, false);
            view.setTag(categoriesList.get(i).getId());

            ((TextView) view.findViewById(R.id.category_box_text_view)).setText(categoriesList.get(i).getName());
            view.findViewById(R.id.category_box_edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditCategoryAlertDialog(v);

                }
            });
            view.findViewById(R.id.category_box_delete_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRemoveAlertDialog(v);
                }
            });

            ((LinearLayout)this.getView().findViewById(R.id.fragment_categories_view_list_list)).addView(view);
            categoriesViewsList.add(view);
        }
    }


    public void showAddNewCategoryAlertDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this.getActivity());
        alertDialog.setMessage(R.string.add_new_category_dialog_msg);
        final EditText input = new EditText(this.getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT );
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setPositiveButton(R.string.dialog_add_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().length() == 0){
                    Toast.makeText(mainActivity, R.string.category_validation_name_too_short, Toast.LENGTH_SHORT).show();
                }else {
                    (new DatabaseHandler(mainActivity)).addNewCategory(new Category(input.getText().toString()));
                    onResume();
                }
            }
        });
        alertDialog.setNegativeButton(R.string.dialog_cancel_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }



    private void showEditCategoryAlertDialog(View v){
        index = -1;
        categorySelectedId = Integer.parseInt(((View)v.getParent()).getTag().toString());
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this.getActivity());
        alertDialog.setMessage(R.string.rename_category_dialog_msg);
        final EditText input = new EditText(this.getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT );
        input.setLayoutParams(lp);
        for(int i = 0; i<categoriesViewsList.size(); i++){
            if(Integer.parseInt(categoriesViewsList.get(i).getTag().toString()) == categorySelectedId){
                input.setText(categoriesList.get(i).getName());
                index = i;
            }
        }
        alertDialog.setView(input);
        alertDialog.setPositiveButton(R.string.dialog_update_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().length() == 0){
                    Toast.makeText(mainActivity, R.string.category_validation_name_too_short, Toast.LENGTH_SHORT).show();
                }else {
                    categoriesList.get(index).setName(input.getText().toString());
                    (new DatabaseHandler(mainActivity)).updateCategory(categoriesList.get(index));
                    onResume();
                }
            }
        });
        alertDialog.setNegativeButton(R.string.dialog_cancel_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void showRemoveAlertDialog(View v) {
        categorySelectedId = Integer.parseInt(((View)v.getParent()).getTag().toString());
        System.out.println("ID: " + ((View) v.getParent()).getTag());
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setMessage(R.string.remove_category_dialog_msg);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.dialog_yes_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                for(int i = 0; i<categoriesViewsList.size(); i++){
                    if(Integer.parseInt(categoriesViewsList.get(i).getTag().toString()) == categorySelectedId){
                        (new DatabaseHandler(mainActivity)).deleteCategory(Integer.parseInt(categoriesViewsList.get(i).getTag().toString()));
                        onResume();
                    }
                }
            }
        });
        alertDialog.setNegativeButton(R.string.dialog_no_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alertDialog.show();
    }

}
