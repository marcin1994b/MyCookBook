package com.marcinbaranski.mycookbook;

import java.util.ArrayList;


public class ShoppingList {

    private int id;
    private String name;
    private ArrayList<Ingredient> ingredientList;

    public ShoppingList(String name, String ingredientList){
        this.name = name;
        this.ingredientList = createShoppingList(ingredientList);
    }

    public ShoppingList(int id, String name, String ingredientList){
        this.id = id;
        this.name = name;
        this.ingredientList = createShoppingList(ingredientList);
    }

    public void setName(String name){
        this.name = name;
    }
    public void setIngredientList(ArrayList<Ingredient> ingredientList){
        this.ingredientList = ingredientList;
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public ArrayList<Ingredient> getList(){
        return this.ingredientList;
    }

    public String getListAsString(){
        String tmp = "";
        for (int i = 0;i<ingredientList.size(); i++) {
            tmp = tmp+ingredientList.get(i).toString()+",";
        }
        return tmp;
    }
    public int getNumberOfIngredients(){
        return ingredientList.size();
    }
    public int getNumberOfBoughtIngredients(){
        int counter = 0;
        for(int i = 0; i<ingredientList.size(); i++){
            if(ingredientList.get(i).isBought()){
                counter++;
            }
        }
        return counter;
    }

    private ArrayList<Ingredient> createShoppingList(String str){
        if(str != null) {
            String[] arr = str.split(",");
            ArrayList<Ingredient> ingredientArrayList = new ArrayList<>();
            int x = 0;
            while(x < arr.length-1){
                ingredientArrayList.add(new Ingredient(arr[x].trim(), Boolean.parseBoolean(arr[x+1].trim())));
                x=x+2;
            }
            return ingredientArrayList;
        }
        return null;
    }


}
