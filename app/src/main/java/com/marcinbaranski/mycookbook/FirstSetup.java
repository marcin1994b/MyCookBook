package com.marcinbaranski.mycookbook;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.view.Display;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Marcin on 22.04.2017.
 */

public class FirstSetup {

    Context context;
    AppCompatActivity appCompatActivity;

    public FirstSetup(Context context, AppCompatActivity appCompatActivity){
        this.context = context;
        this.appCompatActivity = appCompatActivity;
    }

    public void initRecipes(String lang) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();

        InputStream in_s = null;
        if(lang.equals("pl")){
            in_s = context.getAssets().open("plRecipes.xml");
        }else{
            in_s = context.getAssets().open("engRecipes.xml");
        }
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in_s, null);

        ArrayList<Recipe> recipesList = new ArrayList<>();
        int eventType = parser.getEventType();
        Recipe currentRecipe = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equals("recipe")){
                        currentRecipe = new Recipe();
                    } else if (currentRecipe != null){
                        switch (name) {
                            case "name":
                                currentRecipe.setName(parser.nextText());
                                break;
                            case "image":
                                currentRecipe.setImgPath(parser.nextText());
                                break;
                            case "category":
                                currentRecipe.setCategory(parser.nextText());
                                break;
                            case "servs":
                                currentRecipe.setServs(parser.nextText());
                                break;
                            case "prepareTime":
                                currentRecipe.setPreparationTime(parser.nextText());
                                break;
                            case "cookingTime":
                                currentRecipe.setCookingTime(parser.nextText());
                                break;
                            case "ingredients":
                                currentRecipe.setIngredients(parser.nextText());
                                break;
                            case "formula":
                                currentRecipe.setFormula(parser.nextText());
                                break;
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("recipe") && currentRecipe != null){
                        recipesList.add(currentRecipe);
                    }
            }
            eventType = parser.next();
        }

        copyRecipesImages(recipesList);
        addCategories(lang);
    }

    private void copyRecipesImages(ArrayList<Recipe> recipesList){
        long tmp;
        MyBitmap bm = null;
        int imageWidth = getScreenWidth();
        int imageHeight = (int) (context.getResources().getDimension(R.dimen._240sdp));
        for(int i = 0; i < recipesList.size(); i++){
            if(!recipesList.get(i).getImgPath().equals("")){
                switch (recipesList.get(i).getImgPath()){
                    case "spaghetti_bolognese":
                        bm = new MyBitmap(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.spaghetti_bolognese));
                        break;
                    case "strawberry_cake":
                        bm = new MyBitmap(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.strawberry_cake));
                        break;
                    case "carrot_soup":
                        bm = new MyBitmap(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.carrot_soup));
                        break;
                    case "mushroom_soup":
                        bm = new MyBitmap(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.mushroom_soup));
                        break;
                    case "giant_rainbow_cupcake":
                        bm = new MyBitmap(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.giant_rainbow_cupcake));
                        break;
                }
                tmp = System.currentTimeMillis();

                bm.setBitmap(bm.getResizedBitmap(imageWidth, imageHeight));
                recipesList.get(i).setImgPath(bm.saveImgInDirectory("img_" + tmp + ".mycookbook"));

                bm.setBitmap(bm.getResizedBitmap((int)context.getResources().getDimension(R.dimen._100sdp),
                        (int)context.getResources().getDimension(R.dimen.recipe_box_height)));
                bm.saveImgInDirectory("img_" + tmp + ".mycookbookmin");

                addRecipeToDB(recipesList.get(i));
            }
        }
    }

    private void addRecipeToDB(Recipe recipe){
        (new DatabaseHandler(context)).addNewRecipe(recipe);
    }

    private void addCategories(String lang){
        DatabaseHandler db = new DatabaseHandler(context);
        if(lang.equals("pl")){
            db.addNewCategory(new Category("Dania główne"));
            db.addNewCategory(new Category("Ciasta"));
            db.addNewCategory(new Category("Zupy"));
            db.addNewCategory(new Category("Przystawki"));
            db.addNewCategory(new Category("Sałatki"));
            db.addNewCategory(new Category("Jednodaniowe posiłki"));
            db.addNewCategory(new Category("Inne"));
        }else {
            db.addNewCategory(new Category("Main Dishes"));
            db.addNewCategory(new Category("Cakes"));
            db.addNewCategory(new Category("Soups"));
            db.addNewCategory(new Category("Appetizers"));
            db.addNewCategory(new Category("Salads"));
            db.addNewCategory(new Category("Side Dishes"));
            db.addNewCategory(new Category("One Dish Meals"));
            db.addNewCategory(new Category("Beverages"));
            db.addNewCategory(new Category("Other"));
        }
    }


    private int getScreenWidth(){
        Display display = appCompatActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
}
