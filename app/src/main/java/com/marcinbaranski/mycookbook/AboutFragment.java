package com.marcinbaranski.mycookbook;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AboutFragment extends Fragment {

    @InjectView(R.id.contact_box)
    LinearLayout contactButton;
    @InjectView(R.id.rate_box)
    LinearLayout rateButton;

    final static String RATE_BUTTON_TAG = "rateButton";
    final static String CONTACT_BUTTON_TAG = "contactButton";

    private int colorUp;
    private int colorDown;

    Point downFingerPoint;

    public AboutFragment() {
        // Required empty public constructor
    }
    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        colorUp = ContextCompat.getColor(this.getContext(), R.color.layoutColorFirst);
        colorDown = ContextCompat.getColor(this.getContext(), R.color.primary_light);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.about_fragment_toolbar_title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.inject(this, view);
        downFingerPoint = new Point();
        initButtons();
        return view ;
    }

    private void gestureDetector(View v, MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                v.setBackgroundColor(colorDown);
                downFingerPoint.x = (int)event.getRawX();
                downFingerPoint.y = (int)event.getRawY();
                break;
            case MotionEvent.ACTION_UP:
                v.setBackgroundColor(colorUp);
                if(Math.abs(downFingerPoint.x - (int)event.getRawX()) < 1000 &&
                        Math.abs(downFingerPoint.y - (int)event.getRawY()) < 50){
                    if(v.getTag() == CONTACT_BUTTON_TAG){
                        openEmailIntent();
                    }else if(v.getTag() == RATE_BUTTON_TAG){
                        openPlayStorIntent();
                    }
                }
                break;
        }
    }

    private void openEmailIntent(){
        Answers.getInstance().logCustom(new CustomEvent("OpenMailINtent"));
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","marcin1994b@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[MyCookBook]");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private void openPlayStorIntent(){
        Answers.getInstance().logCustom(new CustomEvent("OpenStoreIntent"));
        final String appPackageName = this.getContext().getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void initButtons(){
        contactButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector(v, event);
                return true;
            }});
        contactButton.setTag(CONTACT_BUTTON_TAG);
        rateButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector(v,event);
                return true;
            }
        });
        rateButton.setTag(RATE_BUTTON_TAG);
    }

}
