package com.marcinbaranski.mycookbook;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AddNewShoppingListActivity extends AppCompatActivity {

    @InjectView(R.id.shoppinglist_name_text_edit) EditText name;
    @InjectView(R.id.shoppinglist_list_text_edit) EditText list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_shopping_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_add_shopping_list);
        ButterKnife.inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_new_recipe_menu, menu);
        return true;
    }

    private boolean validation(){
        String validationErrorMsg = "";
        if(name.getText().length() < 5){
            validationErrorMsg += getString(R.string.shopping_list_validation_name_too_short);
        }
        if(list.getText().length() == 0){
            validationErrorMsg += getString(R.string.shoppingList_validation_ingredients_too_short);
        }
        if(!validationErrorMsg.equals("")){
            Toast.makeText(this, validationErrorMsg, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item){
        DatabaseHandler dbHandler = new DatabaseHandler(this);
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.menu_accept_button:
                if(validation()){
                    ShoppingList newShoppingList = new ShoppingList(name.getText().toString(), createList(list.getText().toString()));
                    dbHandler.addNewShoppingList(newShoppingList);
                    Answers.getInstance().logCustom(new CustomEvent("AddShoppingListEvent")
                            .putCustomAttribute("Shopping list name", newShoppingList.getName())
                            .putCustomAttribute("number of ingredients", newShoppingList.getNumberOfIngredients()));
                    this.finish();
                }
                break;
            case R.id.menu_cancel_button:
                this.finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private String createList(String str){
        String[] arr = str.split(",");
        String tmp = "";
        for(int i = 0; i < arr.length; i++){
            System.out.println(arr[i].toString());
            if(!(arr[i].equals("") || arr[i].equals(" "))) {
                if (i == 0) {
                    tmp = arr[i] + ",false";
                } else {
                    tmp = tmp + "," + arr[i] + ",false";
                }
            }
        }
        return tmp;
    }
}
