package com.marcinbaranski.mycookbook;

class Category{

    private int id;
    private String name;

    Category(String name){
        this.name = name;
    }
    Category(int id, String name){
        this.id = id;
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
}
