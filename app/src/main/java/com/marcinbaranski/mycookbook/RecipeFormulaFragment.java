package com.marcinbaranski.mycookbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class RecipeFormulaFragment extends Fragment {

    @InjectView(R.id.recipe_formule_text_view) TextView formulaTextView;

    public RecipeFormulaFragment() {
        // Required empty public constructor
    }

    public static RecipeFormulaFragment newInstance() {
        return new RecipeFormulaFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recipe_formule, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        formulaTextView.setText(getArguments().getString("recipeFormule"));
    }
}
