package com.marcinbaranski.mycookbook;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class MyCookBookActivity extends RuntimePermissionsActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String IS_FIRST_APP_START = "isFirstAppStart";
    private static final String KEY_LANGUAGE_STRING = "languageString";
    private static final String KEY_LANGUAGE_INT = "language";

    private static final String[] TAGS = {"RECIPES_FRAGMENT", "SHOPPINGLISTS_FRAGMENT", "CATEGORIES_FRAGMENT", "SEARCH_IMPORT_FRAGMENT",
                                            "BACKUP_FRAGMENT", "ABOUT_FRAGMENT", "SETTINGS_FRAGMENT"};

    private FloatingActionButton fab;
    private AppCompatActivity context;
    private boolean longPressedMode = false;
    private Fragment activeFragment=null;
    private int REQUEST_PERMISSIONS = 22;
    //private boolean permissionGranted = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, R.string.app_name
                , REQUEST_PERMISSIONS);

        context = this;
        setLanguaage();
        setContentView(R.layout.activity_my_cook_book);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initFloatingActionButton();
        initDrawerLayout(toolbar);
        initNavBar();
        startMainFrgament();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        //Toast.makeText(this, R.string.permissions_received_info, Toast.LENGTH_SHORT).show();
        initFirstSetup();
        if(activeFragment != null) {
            activeFragment.onResume();
        }
    }

    @Override
    public void onPermissionsNotGranted(int requestCode) {
        Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
    }

    public void startMainFrgament(){
        activeFragment = new RecipesViewListFragment().newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_my_cook_book, activeFragment, TAGS[0]);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final int REMOVE_BUTTON = 0;
        final int CANCEL_BUTTON = 1;
        final int SETTINGS_BUTTON = 2;

        getMenuInflater().inflate(R.menu.my_cook_book, menu);
        if(!longPressedMode){
            menu.getItem(REMOVE_BUTTON).setVisible(false);
            menu.getItem(CANCEL_BUTTON).setVisible(false);
            if(activeFragment.getTag().equals( TAGS[0])) {
                menu.getItem(SETTINGS_BUTTON).setVisible(true);
            }else{
                menu.getItem(SETTINGS_BUTTON).setVisible(false);
            }
        }else{
            menu.getItem(REMOVE_BUTTON).setVisible(true);
            menu.getItem(CANCEL_BUTTON).setVisible(true);
            menu.getItem(SETTINGS_BUTTON).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_settings:
                ((RecipesViewListFragment) activeFragment).showSortingSettingsDialog();
                return true;

            case R.id.menu_remove_button:
                if(activeFragment.getTag().equals(TAGS[0])) {
                    ((RecipesViewListFragment) activeFragment).removeButtonPressed();
                }else if(activeFragment.getTag().equals(TAGS[1])){
                    ((ShoppingListsViewsFragment) activeFragment).removeButtonPressed();
                }
                this.invalidateOptionsMenu();
                return true;

            case R.id.menu_cancel_button:
                if(activeFragment.getTag().equals(TAGS[0])){
                    ((RecipesViewListFragment) activeFragment).cancelButtonPressed();
                }else if(activeFragment.getTag().equals(TAGS[1])){
                    ((ShoppingListsViewsFragment) activeFragment).cancelButtonPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDrawerLayout(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }
    private void initNavBar(){
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void initFloatingActionButton(){
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activeFragment.getTag().equals(TAGS[0])) {
                    Intent intent = new Intent(context, AddNewRecipeActivity.class);
                    startActivity(intent);
                }else if(activeFragment.getTag().equals(TAGS[1])){
                    Intent intent = new Intent(context, AddNewShoppingListActivity.class);
                    startActivity(intent);
                }else if(activeFragment.getTag().equals(TAGS[2])){
                    ((CategoriesViewListFragment)activeFragment).showAddNewCategoryAlertDialog();
                }
            }
        });
    }
    
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        String tag = "";
        fab.setVisibility(View.VISIBLE);
        switch(item.getItemId()){
            case R.id.nav_my_recipes:
                activeFragment = RecipesViewListFragment.newInstance();
                tag = TAGS[0];
                break;
            case R.id.nav_shopping_list:
                activeFragment = ShoppingListsViewsFragment.newInstance();
                tag = TAGS[1];
                break;
            case R.id.nav_categories:
                activeFragment = CategoriesViewListFragment.newInstance();
                tag = TAGS[2];
                break;
            case R.id.nav_search_and_import:
                activeFragment = null;
                tag = "";
                break;
            case R.id.nav_backup:
                activeFragment = BackupFragment.newInstance();
                tag = TAGS[4];
                fab.setVisibility(View.INVISIBLE);
                break;
            case R.id.nav_about:
                activeFragment = AboutFragment.newInstance();
                tag = TAGS[5];
                fab.setVisibility(View.INVISIBLE);
                break;
            case R.id.nav_settings:
                activeFragment = SettingsFragment.newInstance();
                tag = TAGS[6];
                fab.setVisibility(View.INVISIBLE);
                break;
        }
        if(activeFragment != null){
            invalidateOptionsMenu();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_my_cook_book, activeFragment, tag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setLongPressedMode(Boolean longPressedMode){
        this.longPressedMode = longPressedMode;
    }
    public Boolean isLongPressedMode(){
        return this.longPressedMode;
    }

    private void setLanguaage(){
        SharedPreferences sharedPreferences = this.getSharedPreferences(getResources().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Locale locale = null;
        if(sharedPreferences.getBoolean(IS_FIRST_APP_START, true)){
            SharedPreferences.Editor editor = sharedPreferences.edit();;
            if(Locale.getDefault().toString().substring(0,2).equals("pl")){
                editor.putString(KEY_LANGUAGE_STRING, "pl");
                editor.putInt(KEY_LANGUAGE_INT, 1);
            }else{
                editor.putString(KEY_LANGUAGE_STRING, "en");
                editor.putInt(KEY_LANGUAGE_INT, 0);
            }
            editor.commit();
        }else {
            locale = new Locale(sharedPreferences.getString(KEY_LANGUAGE_STRING, "en"));
        }
        if(locale != null) {
            Locale.setDefault(locale);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration config = res.getConfiguration();
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                config.setLocale(locale);
                onResume();
            } else {
                config.locale = locale;
            }
            res.updateConfiguration(config, dm);
        }
    }



    private void initFirstSetup(){
        SharedPreferences sharedPreferences = this.getSharedPreferences(getResources().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(IS_FIRST_APP_START, true))
            try {
                (new FirstSetup(this, this)).initRecipes(sharedPreferences.getString(KEY_LANGUAGE_STRING, "en_"));
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(IS_FIRST_APP_START, false);
                editor.commit();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

    }

}
