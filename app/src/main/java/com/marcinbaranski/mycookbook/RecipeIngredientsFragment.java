package com.marcinbaranski.mycookbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;



public class RecipeIngredientsFragment extends Fragment {

    @InjectView(R.id.recipe_ingredients_text_view) TextView ingredientsTextView;

    public RecipeIngredientsFragment() {}

    public static RecipeIngredientsFragment newInstance() {
        return new RecipeIngredientsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ingredientsTextView.setText(getArguments().getString("recipeIngredients"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_ingredients, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

}
