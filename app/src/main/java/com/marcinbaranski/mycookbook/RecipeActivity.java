package com.marcinbaranski.mycookbook;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

public class RecipeActivity extends AppCompatActivity {

    private static final String KEEP_SCREEN_ON = "keepScreenOn";

    AppCompatActivity appCompatActivity;

    Bundle bundle;

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;
        Bundle bundle;

        MyPagerAdapter(FragmentManager fragmentManager, Bundle bundle) {
            super(fragmentManager);
            this.bundle = bundle;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Fragment recipeMainFragment = RecipeMainFragment.newInstance();
                    recipeMainFragment.setArguments(bundle);
                    return recipeMainFragment;
                case 1:
                    Fragment recipeIngredientsFragment = RecipeIngredientsFragment.newInstance();
                    recipeIngredientsFragment.setArguments(bundle);
                    return recipeIngredientsFragment;
                case 2:
                    Fragment recipeFormulaFragment = RecipeFormulaFragment.newInstance();
                    recipeFormulaFragment.setArguments(bundle);
                    return recipeFormulaFragment;
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {

            switch(position){
                case 0:
                    if(lang.equals("pl")){
                        return "Opis";
                    }
                    return "Description";
                case 1:
                    if(lang.equals("pl")){
                        return "Składniki";
                    }
                    return "Ingredients";
                case 2:
                    if(lang.equals("pl")){
                        return "Przepis";
                    }
                    return "Recipe";
            }
            return null;
        }

    }

    static String lang = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_recipe);

        appCompatActivity = this;
        bundle = new Bundle();

        SharedPreferences sharedPreferences = this.getSharedPreferences(getResources().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        setKeepScreenOn(sharedPreferences.getBoolean(KEEP_SCREEN_ON, false));
        lang = sharedPreferences.getString("languageString", "en");

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        FragmentPagerAdapter adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(), bundle);
        vpPager.setAdapter(adapterViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(vpPager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setKeepScreenOn(boolean state){
        if(state){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onResume() {

        DatabaseHandler db = new DatabaseHandler(this);
        int recipeID = getIntent().getIntExtra("recipeID", -1);
        Recipe recipe = db.getRecipe(recipeID);

        bundle.putString("recipeImagePath", recipe.getImgPath());
        bundle.putString("recipeName", recipe.getName());
        bundle.putString("recipeServs", recipe.getServs());
        bundle.putString("recipePreparationTime", recipe.getPreparationTime());
        bundle.putString("recipeCookingTime", recipe.getCookingTime());
        bundle.putString("recipeCategoryName", recipe.getCategory());
        bundle.putString("recipeIngredients", recipe.getIngredients());
        bundle.putString("recipeFormule", recipe.getFormula());

        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_recipe_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.menu_recipe_remove_button:
                createRemoveAlertDialog().show();
                break;
            case R.id.menu_recipe_edit_button:
                Intent intent = new Intent(this, EditRecipeActivity.class);
                intent.putExtra("recipeID", getIntent().getIntExtra("recipeID", -1));
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Dialog createRemoveAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage(R.string.delete_recipe_dialog_msg);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(R.string.dialog_yes_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                DatabaseHandler db = new DatabaseHandler(appCompatActivity);
                db.deleteRecipe(getIntent().getIntExtra("recipeID", -1));
                Answers.getInstance().logCustom(new CustomEvent("RemoveRecipeEvent")
                        .putCustomAttribute("Number of removed recipes", 1));
                finish();
            }
        });
        dialogBuilder.setNegativeButton(R.string.dialog_no_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return dialogBuilder.create();
    }


}
