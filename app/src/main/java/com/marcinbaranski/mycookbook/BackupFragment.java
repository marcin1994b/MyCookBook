package com.marcinbaranski.mycookbook;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class BackupFragment extends Fragment {

    int REQUEST_PERMISSIONS = 20;

    public static String INTERNAL_DB_FILE = "/data/data/com.marcinbaranski.mycookbook/databases/recipesManager";

    @InjectView(R.id.export_fragment_button)
    Button exportButton;
    @InjectView(R.id.import_fragment_button)
    Button importButton;

    @OnClick(R.id.export_fragment_button)
    public void exportButtonListener() {
        showExportDialog();
    }
    @OnClick(R.id.import_fragment_button)
    public void importButtonListener() {
        showImportDialog();
    }

    public BackupFragment() {
        // Required empty public constructor
    }

    public static BackupFragment newInstance() {
        BackupFragment fragment = new BackupFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getContext();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.backup_fragment_toolbar_title);
    }
    Context context;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backup, container, false);
        ButterKnife.inject(this, view);

        ((MyCookBookActivity)this.getActivity()).requestAppPermissions(new
                        String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, R.string.app_name, REQUEST_PERMISSIONS);

        return view;
    }

    private void showImportDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.getContext());
        dialog.setMessage(R.string.backup_import_dialog_msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(R.string.dialog_yes_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    boolean result = (new DatabaseHandler(context)).copyDbFile(Environment.getExternalStorageDirectory().toString() + "/recipesManager", INTERNAL_DB_FILE );
                    if(result) {
                        Toast.makeText(context, R.string.database_imported_success_info, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, R.string.database_imported_failed_info, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        dialog.show();
    }

    private void showExportDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.getContext());
        dialog.setMessage(R.string.backup_export_dialog_msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(R.string.dialog_yes_button_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    boolean result = (new DatabaseHandler(context)).copyDbFile(INTERNAL_DB_FILE, Environment.getExternalStorageDirectory().toString() + "/recipesManager" );
                    if(result) {
                        Toast.makeText(context, R.string.database_exported_success_info, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, R.string.database_exported_failed_info, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();
    }
}
