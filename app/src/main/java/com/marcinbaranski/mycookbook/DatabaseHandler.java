package com.marcinbaranski.mycookbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;

    private static final String DATABASE_NAME = "mycookbook_database";
    private static final String RECIPES_TABLE = "recipes_table";
    private static final String SHOPPINGLIST_TABLE = "shoppingLists_table";
    private static final String CATEGORY_TABLE = "categories_table";

    private static final String KEY_RECIPE_ID = "recipe_id";
    private static final String KEY_RECIPE_NAME = "recipe_name";
    private static final String KEY_RECIPE_CATEGORY_NAME = "recipe_category_name";
    private static final String KEY_RECIPE_SERVS = "recipe_servs";
    private static final String KEY_RECIPE_PREPARATION_TIME = "recipe_preparation_time";
    private static final String KEY_RECIPE_COOKING_TIME = "recipe_cooking_time";
    private static final String KEY_RECIPE_INGREDIENTS = "recipe_ingredients";
    private static final String KEY_RECIPE_NOTE = "recipe_note";
    private static final String KEY_RECIPE_IMG_PATH = "recipe_img_path";

    private static final String KEY_SHOPPINGLIST_ID = "shoppinglist_id";
    private static final String KEY_SHOPPINGLIST_NAME = "shoppinglist_name";
    private static final String KEY_SHOPPINGLIST_CONTENT = "shoppinglist_content";

    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_CATEGORY_NAME = "category_name";

    public static final String ORDER_TYPE_ASC = "ASC";
    public static final String OREDR_TYPE_DESC = "DESC";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createCategoriesTable(db);
        createShoppingListTable(db);
        createRecipesTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RECIPES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SHOPPINGLIST_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE);
        onCreate(db);
    }

    // ---------- Copying method ----------

    boolean copyDbFile(String fromFilePath, String toFilePath) throws IOException {
        close();
        File from = new File(fromFilePath);
        File to = new File(toFilePath);
        if (from.exists()) {
            FileUtils.copyFile(new FileInputStream(from), new FileOutputStream(to));
            getWritableDatabase().close();
            return true;
        }
        return false;
    }

    // ---------- Creating methods ----------

    private void createCategoriesTable(SQLiteDatabase db){
        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + CATEGORY_TABLE + "("
                + KEY_CATEGORY_ID + " INTEGER PRIMARY KEY,"
                + KEY_CATEGORY_NAME + " TEXT)";
        db.execSQL(CREATE_CATEGORIES_TABLE);
    }

    private void createShoppingListTable(SQLiteDatabase db){
        String CREATE_SHOPPINGLISTS_TABLE = "CREATE TABLE " + SHOPPINGLIST_TABLE + "("
                + KEY_SHOPPINGLIST_ID + " INTEGER PRIMARY KEY,"
                + KEY_SHOPPINGLIST_NAME + " TEXT,"
                + KEY_SHOPPINGLIST_CONTENT + " TEXT)";
        db.execSQL(CREATE_SHOPPINGLISTS_TABLE);
    }

    private void createRecipesTable(SQLiteDatabase db){
        String CREATE_RECIPES_TABLE = "CREATE TABLE " + RECIPES_TABLE + "("
                + KEY_RECIPE_ID + " INTEGER PRIMARY KEY,"
                + KEY_RECIPE_NAME + " TEXT,"
                + KEY_RECIPE_CATEGORY_NAME + " TEXT,"
                + KEY_RECIPE_SERVS + " TEXT,"
                + KEY_RECIPE_PREPARATION_TIME + " TEXT,"
                + KEY_RECIPE_COOKING_TIME + " TEXT,"
                + KEY_RECIPE_INGREDIENTS + " TEXT,"
                + KEY_RECIPE_NOTE + " TEXT,"
                + KEY_RECIPE_IMG_PATH + " TEXT)";
        db.execSQL(CREATE_RECIPES_TABLE);
    }

    // ---------- Adding methods ----------

    void addNewRecipe(Recipe recipe){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RECIPE_NAME, recipe.getName());
        values.put(KEY_RECIPE_CATEGORY_NAME, recipe.getCategory());
        values.put(KEY_RECIPE_SERVS, recipe.getServs());
        values.put(KEY_RECIPE_PREPARATION_TIME, recipe.getPreparationTime());
        values.put(KEY_RECIPE_COOKING_TIME, recipe.getCookingTime());
        values.put(KEY_RECIPE_INGREDIENTS, recipe.getIngredients());
        values.put(KEY_RECIPE_NOTE, recipe.getFormula());
        values.put(KEY_RECIPE_IMG_PATH, recipe.getImgPath());

        db.insert(RECIPES_TABLE, null, values);
        db.close();
    }

    void addNewShoppingList(ShoppingList shoppingList){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_SHOPPINGLIST_NAME, shoppingList.getName());
        values.put(KEY_SHOPPINGLIST_CONTENT, shoppingList.getListAsString());

        db.insert(SHOPPINGLIST_TABLE, null, values);
        db.close();
    }

    void addNewCategory(Category category){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_CATEGORY_NAME, category.getName());

        db.insert(CATEGORY_TABLE, null, values);
        db.close();
    }

    // ---------- Getting methods ----------

    Recipe getRecipe(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(RECIPES_TABLE, new String[] { KEY_RECIPE_ID,
                        KEY_RECIPE_NAME, KEY_RECIPE_CATEGORY_NAME, KEY_RECIPE_SERVS, KEY_RECIPE_PREPARATION_TIME,
                        KEY_RECIPE_COOKING_TIME, KEY_RECIPE_INGREDIENTS, KEY_RECIPE_NOTE, KEY_RECIPE_IMG_PATH}, KEY_RECIPE_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return new Recipe(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8));
    }

    ShoppingList getShoppingList(int id){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(SHOPPINGLIST_TABLE, new String[] { KEY_SHOPPINGLIST_ID, KEY_SHOPPINGLIST_NAME,
                        KEY_SHOPPINGLIST_CONTENT}, KEY_SHOPPINGLIST_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if(cursor != null){
            cursor.moveToFirst();
        }
        db.close();
        return new ShoppingList(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
    }

    Category getCategory(int id){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursos = db.query(CATEGORY_TABLE, new String[] { KEY_CATEGORY_ID, KEY_CATEGORY_NAME }, KEY_CATEGORY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        db.close();
        return new Category(Integer.parseInt(cursos.getString(0)), cursos.getString(1));
    }

    // --------- Getting all methods ---------

    ArrayList<Recipe> getAllRecipes(String orderType, String[] categoris){
        ArrayList<Recipe> recipesList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + RECIPES_TABLE;

        if(categoris != null && categoris.length != 0){
            selectQuery = selectQuery + " WHERE ";
            if(categoris.length == 1){
                selectQuery = selectQuery + KEY_RECIPE_CATEGORY_NAME + "=\"" + categoris[0] + "\" ";
            }else{
                for( int i = 0; i< categoris.length; i++){
                    if(i < categoris.length-1) {
                        selectQuery = selectQuery + " " + KEY_RECIPE_CATEGORY_NAME + " = \"" + categoris[i] + "\" OR";
                    }else{
                        selectQuery = selectQuery + " " + KEY_RECIPE_CATEGORY_NAME + " = \"" + categoris[i] + "\" ";
                    }
                }
            }

        }
        selectQuery = selectQuery + " ORDER BY " + KEY_RECIPE_NAME + " " + ORDER_TYPE_ASC;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do {
                Recipe recipe = new Recipe(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6),
                        cursor.getString(7), cursor.getString(8));
                recipesList.add(recipe);
            }while(cursor.moveToNext());
        }
        db.close();
        return recipesList;
    }

    ArrayList<ShoppingList> getAllShoppingLists(){
        ArrayList<ShoppingList> shoppingList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + SHOPPINGLIST_TABLE + " ORDER BY " + KEY_SHOPPINGLIST_NAME + " " + ORDER_TYPE_ASC;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do{
                ShoppingList tmp = new ShoppingList(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2));
                shoppingList.add(tmp);
            }while(cursor.moveToNext());
        }
        db.close();
        return shoppingList;
    }

    ArrayList<Category> getAllCategories(){
        ArrayList<Category> categories = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + CATEGORY_TABLE + " ORDER BY " + KEY_CATEGORY_NAME + " " + ORDER_TYPE_ASC;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do{
                Category tmp = new Category(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1));
                categories.add(tmp);
            }while(cursor.moveToNext());
        }
        db.close();
        return categories;
    }

    // --------- Getting count methods ---------

    int getRecipesCount(){
        String countQuery = "SELECT * FROM " + RECIPES_TABLE;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        db.close();
        return cursor.getCount();
    }

    int getShoppingListsCount(){
        String countQuery = "SELECT * FROM " + SHOPPINGLIST_TABLE;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        db.close();
        return cursor.getCount();
    }

    int getCategoriesCount(){
        String countQuery = "SELECT * FROM " + CATEGORY_TABLE;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        db.close();
        return cursor.getCount();
    }

    // --------- Updating methods ---------

    int updateRecipe(Recipe recipe){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RECIPE_NAME, recipe.getName());
        values.put(KEY_RECIPE_CATEGORY_NAME, recipe.getCategory());
        values.put(KEY_RECIPE_SERVS, recipe.getServs());
        values.put(KEY_RECIPE_PREPARATION_TIME, recipe.getPreparationTime());
        values.put(KEY_RECIPE_COOKING_TIME, recipe.getCookingTime());
        values.put(KEY_RECIPE_INGREDIENTS, recipe.getIngredients());
        values.put(KEY_RECIPE_NOTE, recipe.getFormula());
        values.put(KEY_RECIPE_IMG_PATH, recipe.getImgPath());
        return db.update(RECIPES_TABLE, values, KEY_RECIPE_ID + " = ?",
                new String[] { String.valueOf(recipe.getId()) });
    }

    int updateShoppingList(ShoppingList shoppingList){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SHOPPINGLIST_NAME, shoppingList.getName());
        values.put(KEY_SHOPPINGLIST_CONTENT, shoppingList.getListAsString());
        return db.update(SHOPPINGLIST_TABLE, values, KEY_SHOPPINGLIST_ID + " = ?",
                new String[] { String.valueOf(shoppingList.getId()) });
    }

    int updateCategory(Category category){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_NAME, category.getName());
        return db.update(CATEGORY_TABLE, values, KEY_CATEGORY_ID + " = ?",
                new String[] { String.valueOf(category.getId()) });
    }

    // --------- Deleting methods ---------

    void deleteRecipe(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(RECIPES_TABLE, KEY_RECIPE_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    void deleteShoppingList(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SHOPPINGLIST_TABLE, KEY_SHOPPINGLIST_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    void deleteCategory(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CATEGORY_TABLE, KEY_CATEGORY_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
}

