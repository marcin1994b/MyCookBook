package com.marcinbaranski.mycookbook;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddNewRecipeActivity extends RuntimePermissionsActivity {

    private static final int ACTION_SELECT_FROM_GALLERY = 1;
    private static final int ACTION_CAMERA = 2;

    @InjectView(R.id.recipe_name_edit_text)
    EditText recipeName;
    @InjectView(R.id.servs_edit_text)
    EditText recipeServs;
    @InjectView(R.id.preparation_time_edit_text)
    EditText recipePreparationTime;
    @InjectView(R.id.cooking_time_edit_text)
    EditText recipeCookingTime;
    @InjectView(R.id.ingredients_edit_text)
    EditText recipeIngredients;
    @InjectView(R.id.recipe_edit_text)
    EditText recipe;
    @InjectView(R.id.category_spinner)
    Spinner category;
    @InjectView(R.id.recipe_image_view)
    ImageView recipeImageView;

    private int choosedImageWay = 0;
    private int SELECT_FILE = 1;
    private String recipeImagePath = "";
    private boolean permissionsGranted = false;
    private DatabaseHandler dbHandler = new DatabaseHandler(this);

    @OnClick(R.id.recipe_image_view)
    public void onRecipeImageViewClick(){
        if(permissionsGranted) {
            showImageChooseDialog();
        }else{
            Toast.makeText(this, getString(R.string.add_new_recipe_permissions_granted_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_add_new_recipe);

        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recipeImageView.setImageDrawable(getResources().getDrawable(R.drawable.food_img));
        init();

        int REQUEST_PERMISSIONS = 12;
        requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, R.string.app_name, REQUEST_PERMISSIONS);

    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        permissionsGranted = true;
        Toast.makeText(this, R.string.permissions_received_info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionsNotGranted(int requestCode) {
        Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_new_recipe_menu, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectImageResult(data);
            }
        }
    }

    private boolean validation(){
        String validationErrorMsg = "";
        if(recipeName.getText().length() < 5){
            validationErrorMsg = getString(R.string.recipe_validation_name_too_short);
        }
        if(recipeServs.getText().length() == 0){
            validationErrorMsg += getString(R.string.recipe_validation_servs_too_short);
        }
        if(recipePreparationTime.getText().length() == 0){
            validationErrorMsg += getString(R.string.recipe_validation_prep_time_too_short);
        }
        if(recipeCookingTime.getText().length() == 0){
            validationErrorMsg += getString(R.string.recipe_validation_cookTime_too_short);
        }
        if(!validationErrorMsg.equals("")){
            Toast.makeText(this, validationErrorMsg, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.menu_accept_button:
                if(validation()) {
                    Recipe newRecipe = new Recipe(recipeName.getText().toString(),
                            category.getSelectedItem().toString(),
                            recipeServs.getText().toString(),
                            recipePreparationTime.getText().toString(),
                            recipeCookingTime.getText().toString(),
                            recipeIngredients.getText().toString(),
                            recipe.getText().toString(), recipeImagePath);
                    dbHandler.addNewRecipe(newRecipe);
                    Answers.getInstance().logCustom(new CustomEvent("AddRecipeEvent")
                        .putCustomAttribute("Recipe name", newRecipe.getName())
                        .putCustomAttribute("Recipe category", newRecipe.getCategory()));
                    this.finish();
                }
                break;
            case R.id.menu_cancel_button:
                this.finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init(){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                            categoriesListToStringList((new DatabaseHandler(this.getBaseContext())).getAllCategories()) );
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(spinnerArrayAdapter);

        recipeIngredients.setScroller(new Scroller(this));
        recipeIngredients.setVerticalScrollBarEnabled(true);
        recipeIngredients.setMovementMethod(new ScrollingMovementMethod());

        recipe.setScroller(new Scroller(this));
        recipe.setVerticalScrollBarEnabled(true);
        recipe.setMovementMethod(new ScrollingMovementMethod());
    }

    private ArrayList<String> categoriesListToStringList(ArrayList<Category> arr){
        ArrayList<String> tmp = new ArrayList<>();
        for(int i = 0; i< arr.size(); i++){
            tmp.add(arr.get(i).getName());
        }
        return tmp;
    }

    private void showImageChooseDialog(){
        CharSequence arr[] = this.getResources().getStringArray(R.array.img_choose_dialog);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.add_new_recipe_img_choose_dialog_title);
        dialog.setSingleChoiceItems(arr, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                choosedImageWay = which;
            }
        });
        dialog.setPositiveButton(R.string.menu_accept_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (choosedImageWay){
                    case 0:
                        getImage(ACTION_CAMERA);
                        break;
                    case 1:
                        getImage(ACTION_SELECT_FROM_GALLERY);
                        break;
                }

            }
        });
        dialog.setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        dialog.show();
    }

    private void getImage(int action){
        Intent intent = null;
        if(action == ACTION_CAMERA) {
            intent = new Intent("android.media.action.IMAGE_CAPTURE");
        }else if(action == ACTION_SELECT_FROM_GALLERY){
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }

        if(intent!=null) {
            Uri uriWhereToStore = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/MyCookBookImages/123.jpg"));

            int imageWidth = getScreenWidth();
            int imageHeight = (int) (this.getResources().getDimension(R.dimen._240sdp));

            intent.putExtra("crop", "true");
            intent.putExtra("outputX", imageWidth);
            intent.putExtra("outputY", imageHeight);
            intent.putExtra("aspectX", imageWidth);
            intent.putExtra("aspectY", imageHeight);
            intent.putExtra("scale", false);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriWhereToStore);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

            startActivityForResult(intent, SELECT_FILE);
        }
    }

    private void onSelectImageResult(Intent data) {
        MyBitmap bm=null;
        if (data != null) {
            try {
                long tmp = System.currentTimeMillis();
                bm = new MyBitmap(MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData()));
                recipeImagePath = bm.saveImgInDirectory("img_" + tmp + ".mycookbook");
                bm.setBitmap(bm.getResizedBitmap((int)getResources().getDimension(R.dimen._100sdp), (int)getResources().getDimension(R.dimen.recipe_box_height)));
                bm.saveImgInDirectory("img_" + tmp + ".mycookbookmin");
                recipeImageView.setImageBitmap(bm.getBitmap());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getScreenWidth(){
        Display display = this.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

}
