package com.marcinbaranski.mycookbook;


import android.support.annotation.NonNull;

class Ingredient implements Comparable<Ingredient> {

    private String name;
    private boolean isBought;

    Ingredient(String name, boolean isBought){
        this.name = name;
        this.isBought = isBought;
    }

    void setBought(Boolean isBought){
        this.isBought = isBought;
    }

    public String getName(){
        return this.name;
    }

    boolean isBought(){
        return this.isBought;
    }

    public String toString(){
        return (getName() + "," + isBought());
    }

    @Override
    public int compareTo(@NonNull Ingredient o) {
        return name.compareTo(o.getName());
    }
}
