package com.marcinbaranski.mycookbook;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Marcin on 19.04.2017.
 */

public class MyBitmap {

    public static final String RECIPE_IMG_DIRECTORY = "MyCookBookImages";

    private Bitmap bm;

    public MyBitmap(Bitmap bm) {
        this.bm = bm;
    }
    public MyBitmap(String path){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        File file = new File(path);
        if(file.exists()) {
            this.bm = BitmapFactory.decodeFile(path, options);
        }else {
            this.bm = null;
        }
    }

    public void setBitmap(Bitmap bm){
        this.bm = bm;
    }

    public Bitmap getBitmap(){
        return this.bm;
    }

    public String saveImgInDirectory(String fileName){
        createDirIfNotExists(RECIPE_IMG_DIRECTORY);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        this.bm.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        File destination = new File(Environment.getExternalStorageDirectory() + "/" + RECIPE_IMG_DIRECTORY,
                fileName);
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destination.getPath();
    }

    public Bitmap getResizedBitmap(int newWidth, int newHeight) {
        int width = this.bm.getWidth();
        int height = this.bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(
                this.bm, 0, 0, width, height, matrix, false);
        this.bm.recycle();
        return resizedBitmap;
    }

    private static boolean createDirIfNotExists(String path) {
        boolean ret = true;

        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                ret = false;
            }
        }
        return ret;
    }
}
