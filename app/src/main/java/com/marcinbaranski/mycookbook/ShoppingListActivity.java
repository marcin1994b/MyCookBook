package com.marcinbaranski.mycookbook;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShoppingListActivity extends AppCompatActivity {

    ShoppingList shoppinglist;
    ArrayList<CheckBox> checkboxesList;

    int checkedCheckboxes = 0;

    @InjectView(R.id.shoppinglist_counter_text_view)
    TextView counterTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_shopping_list);
        shoppinglist = new DatabaseHandler(this).getShoppingList(getIntent().getIntExtra("id", -1));
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initFloatingActionButton();

    }

    private void initFloatingActionButton(){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_shopping_list);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openInsertIngAlertDialog();
            }
        });
    }
    private void openInsertIngAlertDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.shopping_list_activity_insert_dialog_msg);
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT );
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setPositiveButton(getString(R.string.dialog_add_button_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateShoppingListDB(input.getText().toString());
                onResume();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        alertDialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshViews();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        System.out.println(keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DatabaseHandler db = new DatabaseHandler(this);
            db.updateShoppingList(shoppinglist);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause(){
        super.onPause();
        DatabaseHandler db = new DatabaseHandler(this);
        db.updateShoppingList(shoppinglist);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_recipe_edit_button:
                break;
            case R.id.menu_recipe_remove_button:
                createRemoveAlertDialog().show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    AppCompatActivity appCompatActivity = this;

    private Dialog createRemoveAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage(R.string.delete_shopping_list_dialog_msg);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(R.string.dialog_yes_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                DatabaseHandler db = new DatabaseHandler(appCompatActivity);
                db.deleteShoppingList(shoppinglist.getId());
                finish();
            }
        });
        dialogBuilder.setNegativeButton(R.string.dialog_no_button_label, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return dialogBuilder.create();
    }

    private void refreshViews(){
        if(checkboxesList != null){
            for(int i = 0; i<checkboxesList.size(); i++){
                ((ViewManager)checkboxesList.get(i).getParent()).removeView(checkboxesList.get(i));
            }
        }
        checkboxesList = new ArrayList<>();
        checkedCheckboxes = 0;
        shoppinglist = new DatabaseHandler(this).getShoppingList(getIntent().getIntExtra("id", -1));
        addCheckboxes(shoppinglist.getList());
        updateCounter();
    }

    private void addCheckboxes(final ArrayList<Ingredient> ingredientsList){
        LinearLayout layout = (LinearLayout) findViewById(R.id.shoppinglist_checkboxes_list);
        for(int i = 0; i < ingredientsList.size(); i++){
            CheckBox checkbox = new CheckBox(this);
            checkbox.setId(i);
            checkbox.setText(ingredientsList.get(i).getName());
            checkbox.setChecked(ingredientsList.get(i).isBought());
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        ingredientsList.get(buttonView.getId()).setBought(true);
                        checkedCheckboxes++;
                    }else{
                        ingredientsList.get(buttonView.getId()).setBought(false);
                        checkedCheckboxes--;
                    }
                    updateCounter();
                }
            });
            if(checkbox.isChecked()){
                checkedCheckboxes++;
            }
            updateCounter();
            layout.addView(checkbox);
            checkboxesList.add(checkbox);
        }

    }

    private void updateCounter(){
        int ingCounter = shoppinglist.getNumberOfIngredients();
        int ingToBuyCounter = ingCounter - shoppinglist.getNumberOfBoughtIngredients();
        String msg = "";

        if(ingToBuyCounter == 0){
            counterTextView.setTextColor(Color.parseColor("#3cc926"));
            msg = getResources().getString(R.string.shopping_list_list_completed_label);
        }else if (ingCounter == 0){
            msg = getResources().getString(R.string.shoppingList_empty_list_label);
        }else{
            counterTextView.setTextColor(Color.BLACK);
            if(ingToBuyCounter == ingCounter){
                msg = getResources().getQuantityString(R.plurals.shopping_list_ingredients_label, ingToBuyCounter, ingToBuyCounter);
            }else{
                msg = getResources().getQuantityString(R.plurals.shopping_list_ingredients_to_buy_label, ingToBuyCounter, ingToBuyCounter);
            }
        }
        counterTextView.setText(msg);
    }

    private void updateShoppingListDB(String str){
        if(!str.equals("")) {
            String[] arr = str.split(",");
            for (String anArr : arr) {
                if(!(anArr.equals(" ") || anArr.equals(""))) {
                    shoppinglist.getList().add(new Ingredient(anArr, false));
                }
            }
            (new DatabaseHandler(this.getBaseContext())).updateShoppingList(shoppinglist);
        }
    }

}
