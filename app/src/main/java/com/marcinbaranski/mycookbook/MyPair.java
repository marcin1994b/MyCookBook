package com.marcinbaranski.mycookbook;

class MyPair<T,E> {

    T first;
    E second;

    MyPair(T first, E second){
        this.first = first;
        this.second = second;
    }
}
