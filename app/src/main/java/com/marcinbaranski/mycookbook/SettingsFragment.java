package com.marcinbaranski.mycookbook;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SettingsFragment extends Fragment {

    private static final String KEEP_SCREEN_ON = "keepScreenOn";
    private static final String EXPORT_DIRECTORY = "exportDirectory";
    private static final String LANGUAGE = "language";
    private static final String LANGUAGE_STRING = "languageString";

    private static final int REQUEST_DIRECTORY = 22;

    @InjectView(R.id.settings_fragment_keep_screen)
    LinearLayout keepScreenOnButton;
    @InjectView(R.id.settings_fragment_export_directory)
    LinearLayout exportDirectoryButton;
    @InjectView(R.id.settings_fragment_language)
    LinearLayout languageButton;
    @InjectView(R.id.settings_fragment_version)
    LinearLayout versionButton;
    @InjectView(R.id.settings_export_directory_box_second_text)
    TextView exportDirectoryTextView;
    @InjectView(R.id.settings_version_box_second_text)
    TextView versionTextView;
    @InjectView(R.id.settings_language_box_second_text)
    TextView languageTextView;
    @InjectView(R.id.settings_keep_screen_box_checkbox)
    CheckBox keepScreenOnCheckBox;

    LinearLayout mainView;

    int colorDown;
    int colorUp;
    int item = 0;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    CharSequence[] languages_array;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.Settings_fragment_toolbar_title);
        mainView = (LinearLayout) inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.inject(this, mainView);
        initSharedPreferences();
        initLanguageArray();
        initColors();
        initButtons();
        setKeepScreenOnCheckBox(sharedPreferences.getBoolean(KEEP_SCREEN_ON, false));
        setLanguageTextView(getLanguageFullName(sharedPreferences.getInt(LANGUAGE, 0)));
        setExportDirectoryTextView(Environment.getExternalStorageDirectory().toString() + "/recipesManager");
        setVersionTextView();
        return mainView;
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshViews();
    }

    private void refreshViews(){
        initButtons();
    }

    public void initButtons(){
        keepScreenOnButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetection(v, event);
            }
        });
        exportDirectoryButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetection(v, event);
            }
        });
        languageButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetection(v, event);
            }
        });
        versionButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetection(v, event);
            }
        });

    }

    private boolean gestureDetection(View v, MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_CANCEL:
                v.setBackgroundColor(colorUp);
                break;
            case MotionEvent.ACTION_DOWN:
                v.setBackgroundColor(colorDown);
                break;
            case MotionEvent.ACTION_UP:
                if(v.equals(keepScreenOnButton)) {
                    if (keepScreenOnCheckBox.isChecked()) {
                        setKeepScreenOnCheckBox(false);
                        setKeepScreenOnSharedPreferences(false);
                    } else {
                        setKeepScreenOnCheckBox(true);
                        setKeepScreenOnSharedPreferences(true);
                    }
                    System.out.println(sharedPreferences.getBoolean(KEEP_SCREEN_ON, false));
                }else if(v.equals(exportDirectoryButton)){

                }else if(v.equals(languageButton)){
                    showLanguageDialog();
                }
                v.setBackgroundColor(colorUp);
                break;
        }
        return true;
    }

    private void setKeepScreenOnSharedPreferences(boolean tmp){
        editor = sharedPreferences.edit();
        editor.putBoolean(KEEP_SCREEN_ON, tmp);
        editor.commit();
    }

    private void showLanguageDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.getContext());
        dialog.setTitle(R.string.settings_fragment_language_dialog_msg);
        dialog.setSingleChoiceItems(languages_array, sharedPreferences.getInt(LANGUAGE, 0), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                item = which;
            }
        });
        dialog.setPositiveButton(R.string.menu_accept_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor = sharedPreferences.edit();
                editor.putInt(LANGUAGE, item);
                editor.putString(LANGUAGE_STRING, getLanguageCode(item));
                editor.commit();
                setAppLanguage(item);
                restartApp();
            }
        });
        dialog.setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    private void setAppLanguage(int language){
        Locale locale = new Locale(getLanguageCode(language));
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = res.getConfiguration();
        if (android.os.Build.VERSION.SDK_INT >= 17){
            config.setLocale(locale);
            onResume();
        } else{
            config.locale = locale;
        }
        res.updateConfiguration(config, dm);
    }

    private String getLanguageCode(int language){
        switch(language){
            case 1:
                return "pl";
            default:
                return "en";
        }
    }

    private String getLanguageFullName(int index){
        return (String)languages_array[index];
    }

    private void setKeepScreenOnCheckBox(boolean state){
        keepScreenOnCheckBox.setChecked(state);
    }

    private void setLanguageTextView(String lang){
        languageTextView.setText(lang);
    }

    private void setExportDirectoryTextView(String path){
        exportDirectoryTextView.setText(path);
    }

    private void restartApp(){
        Intent intent = new Intent(this.getContext(), MyCookBookActivity.class);
        startActivity(intent);
        this.getActivity().finish();
    }

    private void setVersionTextView(){
        try {
            PackageInfo pInfo = this.getContext().getPackageManager().getPackageInfo(this.getContext().getPackageName(), 0);
            String version = pInfo.versionName;
            versionTextView.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void initSharedPreferences(){
        sharedPreferences = this.getActivity().getSharedPreferences(getResources().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    private void initLanguageArray(){
        languages_array = getResources().getStringArray(R.array.languages_array);
    }

    private void initColors(){
        colorUp = ContextCompat.getColor(this.getContext(), R.color.layoutColorFirst);
        colorDown = ContextCompat.getColor(this.getContext(), R.color.primary_light);
    }

}
