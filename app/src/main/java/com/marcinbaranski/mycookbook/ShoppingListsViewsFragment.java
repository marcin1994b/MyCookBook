package com.marcinbaranski.mycookbook;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.ArrayList;


public class ShoppingListsViewsFragment extends Fragment {

    private ArrayList<ShoppingList> shoppingLists;
    private ArrayList<View> shoppingListsView;
    private View currentViewPress;
    private Point downFingerPoint = new Point();
    private int pressedViewCounter = 0;
    private long downTime = 0;
    private int colorUp;
    private int colorDown;

    MyCookBookActivity mainActivity;

    public ShoppingListsViewsFragment() {
        // Required empty public constructor
    }
    public static ShoppingListsViewsFragment newInstance() {
        ShoppingListsViewsFragment fragment = new ShoppingListsViewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainActivity = (MyCookBookActivity) this.getActivity();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.shopping_list_fragment_toolbar_title);
        colorUp = ContextCompat.getColor(this.getContext(), R.color.layoutColorFirst);
        colorDown = ContextCompat.getColor(this.getContext(), R.color.primary_light);
        return inflater.inflate(R.layout.fragment_shopping_lists_views, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshViews();
    }

    private void refreshViews(){
        if(shoppingListsView != null){
            for(int i = 0; i<shoppingListsView.size(); i++){
                ((ViewManager)shoppingListsView.get(i).getParent()).removeView(shoppingListsView.get(i));
            }
        }
        mainActivity.setLongPressedMode(false);
        pressedViewCounter = 0;
        shoppingListsView = new ArrayList<>();
        shoppingLists = new ArrayList<>();
        shoppingLists = (new DatabaseHandler(this.getActivity())).getAllShoppingLists();
        addShoppingListsViews(shoppingLists);
    }

    // remove datas from database, from recipesList and remove views from List and layout.
    public void remove(){
        DatabaseHandler dbHandler = new DatabaseHandler(this.getActivity());
        int tmp = shoppingListsView.size();
        for(int i = 0; i<shoppingListsView.size(); i++){
            if(shoppingListsView.get(i).isPressed()){
                dbHandler.deleteShoppingList(shoppingLists.get(i).getId());
                ((ViewManager)shoppingListsView.get(i).getParent()).removeView(shoppingListsView.get(i));

                shoppingLists.remove(i);
                shoppingListsView.remove(i);
                i--;
            }
        }
        mainActivity.setLongPressedMode(false);
        Answers.getInstance().logCustom(new CustomEvent("RemoveShoppingListsEvent")
                .putCustomAttribute("Number of removed shopping lists", tmp - shoppingListsView.size()));
    }


    private boolean gesturesDetection(View v, MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_CANCEL:
                currentViewPress.setBackgroundColor(colorUp);
                downTime = -1;
                break;
            case MotionEvent.ACTION_DOWN:
                downTime = System.currentTimeMillis();
                downFingerPoint.x = (int)event.getRawX();
                downFingerPoint.y = (int)event.getRawY();
                currentViewPress = v;
                currentViewPress.setBackgroundColor(colorDown);
                break;
            case MotionEvent.ACTION_UP:
                if(Math.abs(downFingerPoint.x - (int)event.getRawX()) < 100 &&
                        Math.abs(downFingerPoint.y - (int)event.getRawY()) < 50){
                    long diff = System.currentTimeMillis() - downTime;

                    if (diff > 800 && !mainActivity.isLongPressedMode()) {
                        mainActivity.setLongPressedMode(true);
                        currentViewPress.setPressed(true);
                        pressedViewCounter++;
                        this.getActivity().invalidateOptionsMenu();
                    } else if(mainActivity.isLongPressedMode() && currentViewPress.isPressed()) {
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(false);
                        pressedViewCounter--;
                        if(pressedViewCounter == 0){
                            mainActivity.setLongPressedMode(false);
                            this.getActivity().invalidateOptionsMenu();
                        }
                    }else if(mainActivity.isLongPressedMode() && !currentViewPress.isPressed()){
                        currentViewPress.setPressed(true);
                        pressedViewCounter++;
                    }else if(!mainActivity.isLongPressedMode()){
                        //This is when your tap is short and start new activity.
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(true);
                        Intent intent = new Intent(this.getActivity(), ShoppingListActivity.class);
                        for(int i = 0; i<shoppingListsView.size(); i++){
                            if(shoppingListsView.get(i).isPressed()){
                                intent.putExtra("id", shoppingLists.get(i).getId());
                            }
                        }
                        currentViewPress.setPressed(false);
                        startActivity(intent);
                    }
                }else{
                    if(!mainActivity.isLongPressedMode()) {
                        currentViewPress.setBackgroundColor(colorUp);
                        currentViewPress.setPressed(false);
                    }
                }
                break;
        }
        return true;
    }

    public void addShoppingListsViews(ArrayList<ShoppingList> shoppingLists){
        LayoutInflater inflater = LayoutInflater.from(this.getActivity());
        for(int i = 0; i < shoppingLists.size(); i++){
            LinearLayout view = (LinearLayout) inflater.inflate(R.layout.shoppinglist_box, null, false);

            ((TextView) view.findViewById(R.id.shoppinglist_name_text_view)).setText(shoppingLists.get(i).getName());
            ((TextView) view.findViewById(R.id.shoppinglist_state_text_view)).setText(getCounter(shoppingLists.get(i)));
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gesturesDetection(v, event);
                }});

            LinearLayout layout = (LinearLayout) this.getView().findViewById(R.id.fragment_shoppingLists_views_list);
            layout.addView(view);

            shoppingListsView.add(view);
        }
    }

    private String getCounter(ShoppingList shoppinglist){
        int ingCounter = shoppinglist.getNumberOfIngredients();
        int ingToBuyCounter = ingCounter - shoppinglist.getNumberOfBoughtIngredients();
        String msg = "";

        if(ingToBuyCounter == 0){
            msg = getResources().getString(R.string.shopping_list_list_completed_label);
        }else if (ingCounter == 0){
            msg = getResources().getString(R.string.shoppingList_empty_list_label);
        }else{
            if(ingToBuyCounter == ingCounter){
                msg = getResources().getQuantityString(R.plurals.shopping_list_ingredients_label, ingToBuyCounter, ingToBuyCounter);
            }else{
                msg = getResources().getQuantityString(R.plurals.shopping_list_ingredients_to_buy_label, ingToBuyCounter, ingToBuyCounter);
            }
        }
        return "( " + msg + " )";
    }

    public void cancelButtonPressed(){
        for(int i = 0; i<shoppingListsView.size(); i++){
            shoppingListsView.get(i).setPressed(false);
            shoppingListsView.get(i).setBackgroundColor(Color.WHITE);
        }
        mainActivity.setLongPressedMode(false);
        mainActivity.invalidateOptionsMenu();
    }
    public void removeButtonPressed(){
        remove();
        onResume();
    }


}
